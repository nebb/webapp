<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Roben</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    
<div class="header space">

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <img src="https://placehold.it/800x300" class="img-fluid" alt="">
        </div>
        <div class="col-md-6">
        <form class="form-inline">
        
        <div class="form-group mx-sm-3 mb-2">
            <!-- <label for="inputPassword2" class="sr-only">Password</label> -->
            <input type="text" class="form-control" id="inputPassword2" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Search</button>
        </form>
        </div>
    </div>
</div>

</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
  </div>
  </div>
</nav>


<div id="content" class="space">

    <div class="container">
        <div class="row">
            <div class="col-md-8 blog">
            

            <!-- start article  -->
            <article>
            <h2>title</h2>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, ipsa ipsam velit reprehenderit in alias ut deserunt eius tempora ea maiores officia amet totam tempore explicabo corrupti provident repellendus. Quos?</p>
            </article>

            <article>
            <h2>title</h2>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, ipsa ipsam velit reprehenderit in alias ut deserunt eius tempora ea maiores officia amet totam tempore explicabo corrupti provident repellendus. Quos?</p>
            </article>
            <!-- end  -->


            </div>

            <div class="col-md-4 sidebar">
            

            <!-- start sidebar -->
            <div class="widget">
            <h4>title</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis eaque cum aliquam fugiat harum. Doloribus excepturi cupiditate, amet delectus minus rem distinctio molestias voluptatem odio enim accusamus, esse, sit iusto!</p>
            </div>

            <div class="widget">
            <h4>title</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis eaque cum aliquam fugiat harum. Doloribus excepturi cupiditate, amet delectus minus rem distinctio molestias voluptatem odio enim accusamus, esse, sit iusto!</p>
            </div>


            <div class="widget">
            <h4>title</h4>
            <p><ul class="list-unstyled">
                <li><a href="#">list</a></li>
                <li><a href="#">list</a></li>
                <li><a href="#">list</a></li>
                <li><a href="#">list</a></li>
                <li><a href="#">list</a></li>
            </ul></p>
            </div>
            <!-- end  -->

            </div>
        </div>
    </div>

</div>

</body>
</html>